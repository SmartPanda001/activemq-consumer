package com.smartpanda.provider.listener;

import org.springframework.jms.annotation.JmsListener;

/**
 * @Author: wujiangwei
 * @Date: 2020/6/24 15:24
 * @Description:
 */
public class TopicConsumerListener {
    //topic模式的消费者
    @JmsListener(destination="${spring.activemq.topic-name}", containerFactory="topicListener")
    public void readActiveQueue(String message) {
        System.out.println("topic接受到：" + message);
    }
}
